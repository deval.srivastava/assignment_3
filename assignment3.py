import pandas as pd
import numpy as np 
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn import decomposition
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RandomizedSearchCV
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import confusion_matrix

def analysis(features,target):
	box = features.boxplot(figsize=(15,15))
	plt.show()
	print(features.describe())
	print(target.count())


def read_data(csv):
	file = pd.read_csv(csv)
	features = file.iloc[:,0:60]
	target=file.iloc[:,-1]
	return features,target



def get_dataset(features,target,split):
	# data = data.to_numpy()
	target = [int(1) if x[0] is 'M' else int(0) for x in target]
	pca = decomposition.PCA(n_components=30,random_state=42)
	pca.fit(features)
	features = pca.transform(features)
	train_features,test_features,train_targets,test_targets = train_test_split(features,target,test_size=split,shuffle=True,random_state=42)
	return (train_features,train_targets),(test_features,test_targets)



def trainer(clf,train):
	print(clf)
	scores = cross_val_score(clf,train[0],train[1],cv=5)
	print("trainer validation",scores.mean())
	clf.fit(train[0],train[1])
	return clf


def grid_tuner(model,paramters,train):
	print(model)
	grid_clf = GridSearchCV(model, param_grid= paramters,scoring = 'roc_auc',n_jobs=8,cv=5)
	grid_clf.fit(train[0], train[1])
	print(grid_clf.best_params_)
	# print(grid_clf.best_estimator_)
	print("validation",grid_clf.best_score_)
	return grid_clf,grid_clf.best_score_


def random_tuner(model,paramters,train):
	print(model)
	grid_clf = RandomizedSearchCV(model, param_distributions= paramters,scoring = 'roc_auc',n_jobs=8,cv=5,n_iter=1000)
	grid_clf.fit(train[0], train[1])
	print(grid_clf.best_params_)
	# print(grid_clf.best_estimator_)
	print("validation",grid_clf.best_score_)
	return grid_clf,grid_clf.best_score_


def error_accuracy(model,test,train):
	 acc_test = model.score(test[0],test[1])
	 acc_train = model.score(train[0],train[1])

	 print("test",acc_test)
	 print("train",acc_train)
	 print("confusion_matrix\n",confusion_matrix(test[1],model.predict(test[0])))
	 return acc_test,acc_train

def scoring(model,test,train):
	values = []
	acc_test = model.score(test[0],test[1])
	acc_train = model.score(train[0],train[1])
	values.extend((acc_test,acc_train))
	pred = model.predict(test[0])
	prec = precision_score(test[1],pred,average='macro')
	recall = recall_score(test[1],pred,average='macro')
	roc = roc_auc_score(test[1],pred)
	f1 = 2*(prec * recall)/(prec+recall)
	values.extend((prec,recall,f1,roc))
	values = ["{0:.2f}".format(a) for a in values]
	return values


def main():
	features,target = read_data("sonar.all-data")
	analysis(features,target)
	train,test = get_dataset(features,target,0.25)


	decision_tree_params={
		"max_depth":np.arange(1,10,1),
		"criterion":["gini","entropy"],
		"min_samples_split":np.arange(0.1,1.0,0.2),
		"min_samples_leaf":np.arange(1,10,2)

	}

	svm_params={
		"C":np.arange(0.1,10,0.1)
		# "gamma":np.arange(1,100,1),
	}

	random_forest_params={
		"n_estimators":np.arange(10,100,10),
		"max_depth":np.arange(1,15,1),
		"min_samples_split":np.arange(2,14,1),
		"min_samples_leaf":np.arange(2,14,1),
		"max_features":list(range(1,train[0].shape[1],1))
	}

	print("1 for decision tree . 2 for random forest . 3 for svm . 4 for all comparison")
	inp = int(input())
	if inp == 1:
		clf = DecisionTreeClassifier(random_state=42)
		clf,_= grid_tuner(clf,decision_tree_params,train)
		error_accuracy(clf,test,train)
	if inp ==2:
		clf = RandomForestClassifier(n_estimators=27, max_depth=7,random_state=42,criterion='gini',min_samples_split=5)
		clf,_ = random_tuner(clf,random_forest_params,train)
		error_accuracy(clf,test,train)
	if inp ==3:
		clf = SVC(C=3.7,gamma=1,kernel='rbf',random_state=42)
		clf,_= grid_tuner(clf,svm_params,train)
		error_accuracy(clf,test,train)
	if inp==4:
		tree_val=["DecisionTree"]
		random_forest_val =["RandomForestClassifier"]
		svm_val =["SVC"]
		tree = DecisionTreeClassifier(random_state=42)
		random_forest = RandomForestClassifier(random_state=42)
		svm = SVC(C=3.7,gamma=1,kernel='rbf',random_state=42)
		tree,temp1 = grid_tuner(tree,decision_tree_params,train)
		tree_val.append("{0:.2f}".format(temp1))
		tree_val.extend(scoring(tree,test,train))
		random_forest,temp2 = random_tuner(random_forest,random_forest_params,train)
		random_forest_val.append("{0:.2f}".format(temp2))
		random_forest_val.extend(scoring(random_forest,test,train))
		svm,temp3 = grid_tuner(svm,svm_params,train)
		svm_val.append("{0:.2f}".format(temp3))
		svm_val.extend(scoring(svm,test,train))
		label = ("name","validation","test","train","precision","recall","f1","roc_auc")
		print(np.vstack((tree_val,random_forest_val,svm_val)).shape)

		ttable = plt.table(cellText=np.vstack((tree_val,random_forest_val,svm_val)),colLabels=label,loc='center',colWidths=[0.05]*8)
		plt.axis("tight")
		plt.axis("off")
		ttable.auto_set_font_size(False)
		ttable.set_fontsize(14)
		ttable.scale(3,3)
		plt.show()




main()